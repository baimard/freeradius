ARG from=ubuntu:20.04

FROM ${from} as builder
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y libpam0g-dev libykclient-dev libykpers-1-dev libyubikey-dev asciidoc-base libmysqlclient-dev gcc make curl libtool autoconf automake pkg-config libpam-dev curl 
RUN apt-get clean ; apt-get autoremove --purge -y
ADD https://github.com/baimard/yubico-pam/archive/master.zip /yubico-pam.zip 

RUN unzip /yubico-pam.zip
RUN rm yubico-pam.zip ; mv yubico-pam-master yubico-pam

WORKDIR /yubico-pam
RUN autoreconf -fi
WORKDIR /opt
RUN /yubico-pam/configure --prefix="$PWD/inst" --without-ldap
RUN make -j8
RUN make install

FROM ${from} as build

RUN apt update && apt full-upgrade -y
RUN apt-get install -y freeradius libpam0g-dev freeradius-mysql libykclient-dev libykpers-1-dev libyubikey-dev libmariadb3 libmysqlclient-dev

# RUN apt-get install -y libpam-yubico

RUN apt-get clean ; apt-get autoremove --purge -y

RUN ln -s /etc/freeradius/3.0/mods-available/pam /etc/freeradius/3.0/mods-enabled/pam
RUN ln -s /etc/freeradius/3.0/mods-available/sql /etc/freeradius/3.0/mods-enabled/sql

RUN rm /etc/freeradius/3.0/sites-enabled/inner-tunnel
RUN rm /etc/freeradius/3.0/mods-enabled/eap

#PAM ADD
COPY --from=builder /opt/inst/lib/security/pam_yubico.so /usr/lib/security/pam_yubico.so

COPY docker-entrypoint.sh /
COPY dictionary /etc/freeradius/3.0/dictionary
RUN chmod 640 /etc/freeradius/3.0/dictionary
RUN chown root:root /docker-entrypoint.sh
RUN chmod 755 /docker-entrypoint.sh

EXPOSE 1812/udp 1813/udp

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["freeradius", "-X"]

# COMPILE cd /home/baimard/Documents/yubicoServeurOtp/build_freeradius && docker build . -t juxo/freeradius-dev && cd /home/baimard/Documents/yubicoServeurOtp/projet_auth/ && docker-compose up -d freeradius && docker-compose logs -f freeradius